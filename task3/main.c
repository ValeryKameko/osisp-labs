#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>
#include <time.h>

#include "processes_structure.h"
#include "concrete_processes_structure.h"

const struct timespec send_delay = {
	.tv_sec = 0,
	.tv_nsec = 1000000
};
pprocess_structure_t process_structure;


void wait_childs(int id) 
{
	for (int i = 0; i < process_structure->processes[id].child_count; i++) {
		int result;
		int child_id = process_structure->processes[id].child_ids[i];
		pid_t child_pid = process_structure->processes[child_id].pid;
		if (child_pid == -1)
			continue;

		pid_t result_pid = waitpid(child_pid, NULL, 0);
		if (result_pid == -1 && errno != ECHILD) {
			printf(
				"[process pid=%6d ppid=%6d #=%d] Error on waiting to [process pid=%6d ppid=%6d #=%d]. Error: %s\n",
				getpid(),
				getppid(),
				id,
				child_pid,
				process_structure->processes[child_id].ppid,
				child_id,
				strerror(errno));
			fflush(stdout);
			exit(1);
		}
	}
}

void destroy_child_processes(int id) 
{
	pid_t pid = getpid();

	for (int i = 0; i < process_structure->processes[id].child_count; i++) {
		int result;
		int child_id = process_structure->processes[id].child_ids[i];
		while (!process_structure->processes[child_id].started);

		pid_t child_pid = process_structure->processes[child_id].pid;
		if (child_pid == -1)
			continue;
		result = kill(
			child_pid,
			SIGTERM);

		if (result != 0) {
			printf(
				"[process pid=%6d ppid=%6d #=%d] Error sending signal \"%s\" to [process pid=%6d ppid=%6d #=%d]. Error: %s\n",
				pid,
				process_structure->processes[id].ppid,
				id,
				strsignal(SIGTERM),
				child_id,
				child_pid,
				process_structure->processes[child_id].ppid,
				strerror(errno));
			fflush(stdout);
			exit(1);
		}
	}
}

void signal_action_handler(int sig, siginfo_t *info, void *ucontext) {
	pid_t pid = getpid();
	int id = get_process_id(process_structure, pid);	

	struct timeval current_time;
	gettimeofday(&current_time, NULL);

	char local_buffer[256];
	char * time_string = ctime_r(&current_time.tv_sec, local_buffer);

	char * new_line = strstr(time_string, "\n");
	*new_line = '\0';

	printf(
		"[process pid=%6d ppid=%6d #=%d] Received signal \"%s\" time: %s (%09d)\n",
		pid,
		process_structure->processes[id].ppid,
		id,
		strsignal(sig),
		time_string,
		(int)current_time.tv_usec);
	fflush(stdout);

	switch (sig)
	{
		case SIGUSR1:
			process_structure->processes[id].usr1_received++;
			break;
		case SIGUSR2:
			process_structure->processes[id].usr2_received++;
			break;
		case SIGTERM:
			printf(
				"[process pid=%6d ppid=%6d #=%d] Destroyed after %ld SIGUSR1 and %ld SIGUSR2\n",
				pid,
				process_structure->processes[id].ppid,
				id,
				process_structure->processes[id].usr1_received,
				process_structure->processes[id].usr2_received);
			fflush(stdout);

			destroy_child_processes(id);
			wait_childs(id);
			
			exit(0);
			break;
		default:
			break;
	}
}

void set_signal_action_handler(int signum)
{
	struct sigaction sa;
	memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_sigaction = signal_action_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_SIGINFO | SA_RESTART;
	int result = sigaction(signum, &sa, NULL);


	int id = get_process_id(process_structure, getpid());
	if (result == -1) {
		printf(
			"[process pid=%6d ppid=%6d #=%d] Error on set signal handler for %s. Error: %s\n",
			id,
			process_structure->processes[id].pid,
			process_structure->processes[id].ppid,
			strsignal(signum),
			strerror(errno));
		fflush(stdout);
		exit(1);
	}
}

void process_function(int id) 
{
	pprocess_description_t process_description = 
		&process_structure->processes[id];

	printf("[process pid=%6d ppid=%6d #=%d] Created\n", id, getpid(), getppid());

	fflush(stdout);

	lock_process_structure(process_structure);

	process_description->pid = getpid();
	process_description->ppid = getppid();
	process_description->started = true;

	unlock_process_structure(process_structure);

	printf(
		"[process pid=%6d ppid=%6d #=%d] Initialized\n",
		process_description->pid, 
		process_description->ppid,
		id);
	fflush(stdout);

	for (int i = 0; i < process_description->child_count; i++) {
		int child_id = process_description->child_ids[i];

		pid_t pid = fork();
		if (pid == -1) {
			printf(
				"[process pid=%6d ppid=%6d #=%d] Error on fork process #%d. Error: %s\n",
				process_description->pid,
				process_description->ppid,
				id,
				child_id, 
				strerror(errno));
			fflush(stdout);
			exit(1);
		}
		if (pid != 0)
			continue;

		process_function(child_id);	
	}
	
	const pprocess_communication_entry_t communication =
		process_description->communication;

	set_signal_action_handler(SIGUSR1);
	set_signal_action_handler(SIGUSR2);
	set_signal_action_handler(SIGTERM);

	process_structure->processes[id].ready_to_receive = true;

	printf(
		"[process pid=%6d ppid=%6d #=%d] Ready to receive\n",
		process_description->pid,
		process_description->ppid,
		id);
	fflush(stdout);
	if (communication) {
		for (int j = 0; j < communication->sender_count; j++) {
			int sender_id = communication->sender_ids[j];
			
			bool need_wait = true;
			while (!process_structure->processes[sender_id].ready_to_receive);
		}

		printf(
			"[process pid=%6d ppid=%6d #=%d] Start sending\n",
			process_description->pid,
			process_description->ppid,
			id);
		fflush(stdout);

		for (int i = 0; i < process_structure->processes[id].message_count; i++) {
			for (int j = 0; j < communication->sender_count; j++) {
				int sender_id = communication->sender_ids[j];
				pid_t sender_pid = process_structure->processes[id].pid;

				printf(
					"[process pid=%6d ppid=%6d #=%d] Send signal \"%s\" to [process pid=%6d ppid=%6d #=%d]\n",
					process_description->pid,
					process_description->ppid,
					id,
					strsignal(communication->signal_type),
					sender_pid,
					process_structure->processes[sender_id].ppid,
					sender_id);
				fflush(stdout);
				
				if (sender_pid == -1) {
					printf(
						"[process pid=%6d ppid=%6d #=%d] notice die of #%d\n",
						process_description->pid,
						process_description->ppid,
						id,
						sender_id);
					fflush(stdout);
					exit(1);
				}
				int result = kill(
					sender_pid,
					communication->signal_type);

				if (result != 0) {
					printf(
						"[process pid=%6d ppid=%6d #=%d] Error on sending signal \"%s\" to [process pid=%6d ppid=%6d #=%d]\n",
						process_description->pid,
						process_description->ppid,
						id,
						strsignal(communication->signal_type),
						sender_id,
						sender_pid,
						process_structure->processes[sender_id].ppid);
					fflush(stdout);
					exit(1);
				}
			}

			int result;
			struct timespec remaining_time;
			remaining_time = send_delay;
			while ((result = nanosleep(&remaining_time, &remaining_time)) && errno == EINTR);
			if (result) {
				printf(
					"[process pid=%6d ppid=%6d #=%d] Error on nanosleep. Error: %s\n",
					process_description->pid,
					process_description->ppid,
					id,
					strerror(errno));
				fflush(stdout);
				exit(1);
			}
		}
	}

	if (id != 0) {
		destroy_child_processes(id);
		wait_childs(id);
		exit(0);
	}
}

int main(int argv, char *argc) 
{
	process_structure = create_process_structure(
		process_count,
		(const pprocess_tree_node_t)process_tree);

	process_function(0);

	for (int i = 0; i < process_structure->processes[0].child_count; i++) {
		int id = process_structure->processes[0].child_ids[i];
		while (!process_structure->processes[id].started);
	}
	wait_childs(0);

	destroy_process_structure(process_structure);
	return 0;
}
