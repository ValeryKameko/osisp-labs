#ifndef PROCESS_STRUCTURE_H
#define PROCESS_STRUCTURE_H

#include <stdint.h>
#include <stdlib.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdatomic.h>

typedef struct {
    size_t sender_count;
    int * sender_ids;
    int signal_type;
} process_communication_entry_t, *pprocess_communication_entry_t;

typedef struct {      
    int id;
    volatile pid_t pid, ppid;

    size_t child_count;
    int * child_ids;

    pprocess_communication_entry_t communication;

    size_t message_count;
    volatile bool ready_to_receive;
    volatile bool started;
    volatile atomic_size_t usr1_received, usr2_received;
} process_description_t, *pprocess_description_t;

typedef struct {
    sem_t lock;
    size_t process_count;
    pprocess_description_t processes;
} process_structure_t, *pprocess_structure_t;

typedef struct {
    size_t child_count;
    int * childs;
    pprocess_communication_entry_t communication;
    size_t message_count;
} process_tree_node_t, *pprocess_tree_node_t;

pprocess_structure_t create_process_structure(
    size_t process_count,
    const pprocess_tree_node_t process_tree);
int get_process_id(pprocess_structure_t process_structure, pid_t pid);
void lock_process_structure(pprocess_structure_t process_structure);
void unlock_process_structure(pprocess_structure_t process_structure);

void destroy_process_structure(pprocess_structure_t process_structure);

#endif // PROCESS_STRUCTURE_H
