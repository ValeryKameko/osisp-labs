#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#include <stddef.h>

void * allocate_shared_memory(size_t size);

void deallocate_shared_memory(void * memory, size_t size);

#endif // SHARED_MEMORY_H