#ifndef CONCRETE_PROCESSES_STRUCTURE_H
#define CONCRETE_PROCESSES_STRUCTURE_H

#include "processes_structure.h"

/* Prcess tree:
    1 -> (2, 3, 4, 5)
    2 -> 6
    3 -> 7
    4 -> 8
*/

/* Communication:
    1 -> 2  SIGUSR1
    2 -> (3, 4) SIGUSR2
    4 -> 5 SIGUSR1
    3 -> 6 SIGUSR1
    6 -> 7 SIGUSR1
    7 -> 8 SIGUSR1
    8 -> 1 SIGUSR2
*/

const extern size_t process_count;
const extern process_tree_node_t process_tree[];

#endif // CONCRETE_PROCESS_STRUCTURE_H