#include "shared_memory.h"
#include <errno.h>
#include <stdio.h>
#include <sys/mman.h>
#include <string.h>
#include <stdlib.h>

void * allocate_shared_memory(size_t size) 
{
	void * memory = NULL;
	if (size != 0) {
		memory = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
		if (!memory) {
			printf("Cannot allocate %d bytes of shared memory. Error: %s\n", (int)size, strerror(errno));
			fflush(stdout);
			exit(1);
		}
	}
	return memory;
}

void deallocate_shared_memory(void * memory, size_t size) 
{
    int result = munmap(memory, size);
    if (result) {
        printf("Cannot deallocate %d. Error: %s\n", (int)size, strerror(errno));
		fflush(stdout);
		exit(1);
    }
}