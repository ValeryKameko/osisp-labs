#include "processes_structure.h"
#include "shared_memory.h"
#include <semaphore.h>
#include <string.h>

pprocess_structure_t create_process_structure(
    size_t process_count,
    const pprocess_tree_node_t process_tree)
{
    pprocess_structure_t structure;
    structure = allocate_shared_memory(sizeof(process_structure_t));

    structure->process_count = process_count;
    structure->processes = allocate_shared_memory(
        process_count * sizeof(process_description_t));

    for (int i = 0; i < process_count; i++) {
        size_t child_count = process_tree[i].child_count;
        int * child_ids = allocate_shared_memory(
            child_count * sizeof(int));

        memcpy(
            child_ids, 
            process_tree[i].childs, 
            child_count * sizeof(int));

        pprocess_communication_entry_t communication = NULL;
        if (process_tree[i].communication) {
            communication = allocate_shared_memory(
                sizeof(process_communication_entry_t));
            
            size_t sender_count = process_tree[i].communication->sender_count;
            int * sender_ids = allocate_shared_memory(sizeof(int) * sender_count);

            memcpy(
                sender_ids, 
                process_tree[i].communication->sender_ids,
                sizeof(int) * sender_count);

            *communication = (process_communication_entry_t){
                .sender_count = sender_count,
                .sender_ids = sender_ids,
                .signal_type = process_tree[i].communication->signal_type
            };
        }

        structure->processes[i] = (process_description_t){
            .id = i,
            .pid = -1,
            .ppid = -1,
            .child_count = process_tree[i].child_count,
            .child_ids = child_ids,
            .communication = communication,
            .message_count = process_tree[i].message_count,
            .ready_to_receive = false,
            .started = false
        };
    }

    sem_init(&structure->lock, 1, 1);
    return structure;
}

void lock_process_structure(pprocess_structure_t process_structure)
{
    sem_wait(&process_structure->lock);
}

void unlock_process_structure(pprocess_structure_t process_structure)
{
    sem_post(&process_structure->lock);
}

int get_process_id(pprocess_structure_t process_structure, pid_t pid)
{
    int id = -1;
    lock_process_structure(process_structure);

    for (int i = 0; i < process_structure->process_count; i++) {
        if (process_structure->processes[i].pid == pid) {
            id = process_structure->processes[i].id;
            break;
        }
    }

    unlock_process_structure(process_structure);
    return id;
}


void destroy_process_structure(pprocess_structure_t process_structure)
{
    sem_destroy(&process_structure->lock);

    for (int i = 0; i < process_structure->process_count; i++) {
        if (process_structure->processes[i].child_ids) {
            deallocate_shared_memory( 
                process_structure->processes[i].child_ids,
                process_structure->processes[i].child_count * sizeof(int));
        }
            
        if (process_structure->processes[i].communication) {
            if (process_structure->processes[i].communication->sender_ids) {
                deallocate_shared_memory(
                    process_structure->processes[i].communication->sender_ids,
                    process_structure->processes[i].communication->sender_count * sizeof(int));
            }
            if (process_structure->processes[i].communication) {
                deallocate_shared_memory(
                    process_structure->processes[i].communication,
                    sizeof(process_communication_entry_t));
            }
        }
    }

    deallocate_shared_memory(
        process_structure->processes,
        process_structure->process_count * sizeof(process_description_t));

    deallocate_shared_memory(
        process_structure,
        sizeof(process_structure_t));
}