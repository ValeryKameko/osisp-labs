#include "concrete_processes_structure.h"
#include <signal.h>

#define PROCESS_COUNT 9

const size_t process_count = PROCESS_COUNT;

const process_tree_node_t process_tree[PROCESS_COUNT] = {
    { // process 0
        .child_count = 1,
        .childs = (int []){ 1 },
        .communication = NULL,
        .message_count = 0
    },
    { // process 1
        .child_count = 4,
        .childs = (int []){ 2, 3, 4, 5 },
        .communication = &(process_communication_entry_t) {
            .sender_count = 1,
            .sender_ids = (int []){ 2 },
            .signal_type = SIGUSR1
        },
        .message_count = 101
    },
    { // process 2
        .child_count = 1,
        .childs = (int []){ 6 },
        .communication = &(process_communication_entry_t) {
            .sender_count = 2,
            .sender_ids = (int []){ 3, 4 },
            .signal_type = SIGUSR2
        },
        .message_count = 101
    },
    { // process 3
        .child_count = 1,
        .childs = (int []){ 7 },
        .communication = &(process_communication_entry_t) {
            .sender_count = 1,
            .sender_ids = (int []){ 6 },
            .signal_type = SIGUSR1
        },
        .message_count = 101
    },
    { // process 4
        .child_count = 1,
        .childs = (int []){ 8 },
        .communication = &(process_communication_entry_t) {
            .sender_count = 1,
            .sender_ids = (int []){ 5 },
            .signal_type = SIGUSR1
        },
        .message_count = 101
    },
    { // process 5
        .child_count = 0,
        .childs = (int []){ },
        .communication = NULL,
        .message_count = 101
    },
    { // process 6
        .child_count = 0,
        .childs = (int []){ },
        .communication = &(process_communication_entry_t) {
            .sender_count = 1,
            .sender_ids = (int []){ 7 },
            .signal_type = SIGUSR1
        },
        .message_count = 101
    },
    { // process 7
        .child_count = 0,
        .childs = (int []){ },
        .communication = &(process_communication_entry_t) {
            .sender_count = 1,
            .sender_ids = (int []){ 8 },
            .signal_type = SIGUSR1
        },
        .message_count = 101
    },
    { // process 8
        .child_count = 0,
        .childs = (int []){ },
        .communication = &(process_communication_entry_t) {
            .sender_count = 1,
            .sender_ids = (int []){ 1 },
            .signal_type = SIGUSR2
        },
        .message_count = 101
    },
};