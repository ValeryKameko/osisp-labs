#!/bin/bash

dir=$1
count=$2

rm -r $dir &> /dev/null
mkdir $dir &> /dev/null

for (( i = 1; i <= count; i++ )); do
	(( random_number=RANDOM%10 ))
	echo "$random_number" > "$dir/${i} with value ${random_number}" 
done
