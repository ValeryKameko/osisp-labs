#!/bin/bash

# Variant = 4

dir1=$1
dir2=$2

compares_count=0

if (( $# != 2 )); then
	echo "Must be 2 arguments"
	exit 1
fi

if ! [ -d $dir1 ]; then
	echo "Directory $dir1 is not reachable!"
	exit 1
elif [ -z $dir1 ]; then
	echo "Directory name must be non-empty"
	exit 1
fi	

if ! [ -d $dir2 ]; then
	echo "Directory $dir2 is not reachable!"
	exit 1
elif [ -z $dir2 ]; then
	echo "Directory name must be non-empty"
	exit 1
fi

files1=$(find $dir1 -type f &2> /dev/null)
files2=$(find $dir2 -type f &2> /dev/null)

IFS=$'\n'
for file1 in $files1; do
	if ! [ -r $file1 ]; then
		echo "file \"$file1\" is not reachable"
		continue
	fi
	IFS=$'\n'
	for file2 in $files2; do
		if ! [ -r $file2 ]; then
			echo "file \"$file2\" is not reachable"
			continue
		fi
		(( compares_count++ ))
		compares_padded=$(printf "%03d" $compares_count)
		if ! $(diff $file1 $file2 &>/dev/null) ; then
			echo "(compare=$compares_padded) files \"$file1\" and \"$file2\": ----different----"
		else
			echo "(compare=$compares_padded) files \"$file1\" and \"$file2\": ++++++same+++++++"
		fi
	done
done
