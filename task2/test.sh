#!/bin/bash

function padding() {
	local padding_count=$1
	local i
	for (( i = 0; i < padding_count; i++)); do
		echo "     "
	done	
}

function create_random_hierarchy() {
	local current_dir=$1
	local files_count=$2
	local depth=$3
	
	local current_dir_files_count=$(( RANDOM % files_count ))
	echo "\$DEBUG\$ current_dir_files_count = $current_dir_files_count"
	echo "----- depth = $depth" >&2
	if (( depth <= 0 )); then
		current_dir_files_count=$files_count
	fi
	local i=1
	for (( i = 1; i <= current_dir_files_count; i++ )); do
		local extension_id=$(( RANDOM % ${#extensions[*]} ))
		local file_path="$current_dir/file_$i.${extensions[$extension_id]}"
		local file_size=$(( RANDOM % (max_file_size + 1) ))
		echo "\$DEBUG\$ i = $i"
		touch $file_path
		echo "\$DEBUG\$ created"
		echo "\$DEBUG\$ writing $file_size bytes to $file_path"
		dd if=/dev/urandom of=$file_path count=$file_size bs=1 
		echo "\$DEBUG\$ wrote"
		(( files_count-- ))
		echo "created file $file_path"
	done
	local i=0
	while (( files_count > 0 )); do
		(( i++ ))
		local new_dir_files_count=$(( RANDOM % files_count + 1 ))
		(( files_count-=new_dir_files_count ))
		local new_dir="$current_dir/dir_$i"
		mkdir $new_dir
		echo "created dir $new_dir"
		create_random_hierarchy $new_dir $new_dir_files_count $(( depth - 1 ))
	done
}

dir=$1
count=$2
max_depth=$3
max_file_size=512
extensions=(exe txt lib so rtf docx)

if [ -z "$max_depth" ]; then
	max_depth="3"
fi

rm -r $dir &> /dev/null
mkdir $dir

create_random_hierarchy $dir $count $max_depth
