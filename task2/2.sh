#!/bin/bash

#Variant 9

dir=$1
extension=$2
new_dir=$3

if (( $# != 3 )); then
	echo "Must be 3 arguments"
	exit 1
fi

if ! [ -d $dir ]; then
	echo "$dir is not reachable" >&2
	exit 1
elif [ -z $dir ]; then
	echo "Directory must be non-empty"
	exit 1
fi

if [ -z $extension ]; then
	echo "Extension must be non-empty" 
	exit 1
fi

if [ -e $new_dir ]; then
	echo "$new_dir already exists"
	exit 1
elif [ -z $new_dir ]; then
	echo "Directory must be non-empty"
	exit 1
fi

files=$(find $dir -name "*.$extension")

IFS=$'\n'
for file in $files; do
	relative_file_path=${file##$dir}
	link_path="$new_dir/$relative_file_path"
	echo "\$DEBUG\$ dir = $dir"
	echo "\$DEBUG\$ file = $file"
	echo "\$DEBUG\$ relative_file_path = $relative_file_path"
	echo "\$DEBUG\$ creating dir $(dirname $link_path)"
	mkdir -p $(dirname $link_path)
	ln $file $link_path 
done
