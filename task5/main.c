#include "master_process.h"

int main(int argc, char **argv) {
    pmaster_process_t descriptor = master_process_create(2);
    master_process_start(descriptor);
    master_process_destroy(descriptor);
    return 0;
}