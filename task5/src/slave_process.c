#include "slave_process.h"
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <signal.h> 
#include <fcntl.h>
#include <sys/stat.h>
#include <sched.h>

#define BUFFER_SIZE 1024
#define TIME_BUFFER_SIZE 256

static pslave_process_t global_descriptor;

void start_handler() {
    global_descriptor->started = true;
}

void setup_start_signal_handler(int signum) {
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_sigaction = &start_handler;

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_SIGINFO | SA_RESTART;
	int result = sigaction(signum, &sa, NULL);
    if (result != 0)
        error(1, errno, "Cannot set signal handler for %s\n", strsignal(signum));
}

pslave_process_t slave_process_create(
    char *file_path,
    size_t lines_count,
    size_t lines_batch,
    sem_t *file_lock,
    sem_t *intialized_lock,
    sem_t *events,
    volatile int *line_count) {

    pslave_process_t descriptor = (pslave_process_t) malloc(sizeof(slave_process_t));
    global_descriptor = descriptor;
    setup_start_signal_handler(SIGUSR2);

    descriptor->file_fd = open(file_path, O_WRONLY | O_APPEND);
    descriptor->lines_left = lines_count;
    descriptor->lines_batch = lines_batch;
    descriptor->file_lock = file_lock;
    descriptor->pid = getpid();
    descriptor->started = false;
    descriptor->line_count = line_count;
    descriptor->events = events;

    sem_post(intialized_lock);
    return descriptor;
}

void slave_process_start(pslave_process_t descriptor) {
    while (!descriptor->started)
        pause();
    printf("[process %u] slave START!\n", getpid());
    fflush(stdout);

    char line_buffer[BUFFER_SIZE];
    char time_buffer[TIME_BUFFER_SIZE];
    struct timeval current_time;

    size_t line_id = 0;
    
    while (descriptor->lines_left) {
        size_t current_line_count = descriptor->lines_left < descriptor->lines_batch ?
            descriptor->lines_left :
            descriptor->lines_batch;
        sem_wait(descriptor->file_lock);

        descriptor->lines_left -= current_line_count;
        *(descriptor->line_count) += current_line_count;

        while (current_line_count) {
            gettimeofday(&current_time, NULL);

            sprintf(
                line_buffer,
                "line #%4lu, process #%5d, time %09d mksec\n",
                line_id,
                descriptor->pid,
                (int)current_time.tv_usec);

            write(descriptor->file_fd, line_buffer, (size_t) strlen(line_buffer));

            line_id++;
            current_line_count--;
        }

        if (descriptor->lines_left == 0) {
            kill(getppid(), SIGUSR1);
        }

        sem_post(descriptor->events);    
        sem_post(descriptor->file_lock);

        sleep(0);
    }

    printf("[process %u] slave END!\n", getpid());
    fflush(stdout);
}

void slave_process_destroy(pslave_process_t descriptor) {
    close(descriptor->file_fd);
    free(descriptor);
    exit(0);
}