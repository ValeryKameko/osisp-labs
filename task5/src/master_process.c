#define _GNU_SOURCE 

#include <sys/stat.h>
#include <fcntl.h>
#include "master_process.h"
#include "slave_process.h"
#include <malloc.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <error.h>
#include <errno.h>

#define TASK_LINE_COUNT 1100
#define TASK_LINE_BATCH 3

#define CONSUMER_LINE_BATCH 3

static pmaster_process_t global_descriptor;

void finish_handler(int signum, siginfo_t *info, void *ucontext) {
    global_descriptor->processes_finished++;
    sem_post(global_descriptor->events);
    printf("[process %ul] child finished received!\n", getpid());
    fflush(stdout);
}

void setup_finish_handler(int signum) {
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_sigaction = &finish_handler;

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_SIGINFO | SA_RESTART;
	int result = sigaction(signum, &sa, NULL);
    if (result != 0)
        error(1, errno, "Cannot set signal handler for %s\n", strsignal(signum));
}

pmaster_process_t master_process_create(size_t child_count) {
    pmaster_process_t descriptor = (pmaster_process_t) malloc(sizeof(master_process_t));

    descriptor->child_count = child_count;
    descriptor->file_lock = (sem_t*) mmap(
        NULL,
        sizeof(sem_t),
        PROT_READ | PROT_WRITE,
        MAP_SHARED | MAP_ANONYMOUS,
        -1,
        0
    );
    sem_init(descriptor->file_lock, 1, 1);

    descriptor->child_initialized_lock = (sem_t*) mmap(
        NULL,
        sizeof(sem_t),
        PROT_READ | PROT_WRITE,
        MAP_SHARED | MAP_ANONYMOUS,
        -1,
        0
    );
    sem_init(descriptor->child_initialized_lock, 1, 0);

    char * temp_file_name = strdup("/tmp/shared_temp_file.XXXXXX");
    temp_file_name = mktemp(temp_file_name);
    descriptor->file_fd = open(temp_file_name, O_CREAT | O_TRUNC | O_RDWR);
    descriptor->child_pids = (pid_t *) calloc(child_count, sizeof(pid_t));
    descriptor->events = (sem_t*) mmap(
        NULL,
        sizeof(sem_t),
        PROT_READ | PROT_WRITE,
        MAP_SHARED | MAP_ANONYMOUS,
        -1,
        0
    );
    sem_init(descriptor->events, 1, 0);

    descriptor->line_count = (volatile int*) mmap(
        NULL,
        sizeof(int),
        PROT_READ | PROT_WRITE,
        MAP_SHARED | MAP_ANONYMOUS,
        -1,
        0
    );
    *(descriptor->line_count) = 0;

    global_descriptor = descriptor;
    setup_finish_handler(SIGUSR1);  

    for (int i = 0; i < child_count; i++) {
        pid_t child_pid = fork();
        if (child_pid != 0) {
            descriptor->child_pids[i] = child_pid;
            if (i == 0)
                descriptor->group_id = child_pid;
            setpgid(child_pid, descriptor->group_id);
            continue;
        }
        pslave_process_t slave_process = slave_process_create(
            temp_file_name,
            TASK_LINE_COUNT,
            TASK_LINE_BATCH,
            descriptor->file_lock,
            descriptor->child_initialized_lock,
            descriptor->events,
            descriptor->line_count);
        slave_process_start(slave_process);
        slave_process_destroy(slave_process);
    }
    
    return descriptor;
}

char * read_line(int fd) {
    static const int BUFFER_SIZE = 64;

    int current_pos = 0;
    int current_buffer_size = BUFFER_SIZE;
    char * buffer = (char *) malloc(current_buffer_size);

    while (true) {
        while (current_pos + 2 >= current_buffer_size) {
            current_buffer_size = 2 * current_buffer_size;
            buffer = realloc(buffer, current_buffer_size);
        }
        char ch;
        ssize_t read_count = read(fd, &ch, 1);
        buffer[current_pos++] = ch;
        if (ch == '\n') {
            buffer[current_pos++] = '\0';
            break;
        }
        if (read_count == 0)
            exit(2);
    }
    return buffer;
}

void master_process_start(pmaster_process_t descriptor) {
    for (int i = 0; i < descriptor->child_count; i++) 
        sem_wait(descriptor->child_initialized_lock);
    printf("[process %u] master START!\n", getpid());
    fflush(stdout);
    killpg(descriptor->group_id, SIGUSR2);

    int batch_id = 0;
    while (true) {
        sem_wait(descriptor->events);
        sem_wait(descriptor->file_lock);

        int current_line_count = CONSUMER_LINE_BATCH;
        if (current_line_count > *(descriptor->line_count))
            current_line_count = *(descriptor->line_count);

        if (current_line_count != CONSUMER_LINE_BATCH && descriptor->processes_finished != descriptor->child_count) {
            sem_post(descriptor->file_lock);
            continue;
        }
        if (current_line_count == 0)
            break;

        *(descriptor->line_count) -= current_line_count;

        printf("Batch #%d:\n", batch_id);
        fflush(stdout);
        batch_id++;

        while (current_line_count) {
            char * line = read_line(descriptor->file_fd);
            printf("%s", line);
            current_line_count--;
        }
        puts("");
        fflush(stdout);

        if (current_line_count == 0 && descriptor->processes_finished == descriptor->child_count) {
            sem_post(descriptor->events);
        }

        sem_post(descriptor->file_lock);
    }
    printf("[process %ul] master END!\n", getpid());
    fflush(stdout);
}

void master_process_destroy(pmaster_process_t descriptor) {
    while (descriptor->processes_finished < descriptor->child_count)
        pause();
    
    for (int i = 0; i < descriptor->child_count; i++)
        wait(NULL);

    sem_close(descriptor->file_lock);
    munmap(descriptor->file_lock, sizeof(sem_t));

    sem_close(descriptor->child_initialized_lock);
    munmap(descriptor->child_initialized_lock, sizeof(sem_t));

    sem_close(descriptor->events);
    munmap(descriptor->events, sizeof(sem_t));

    munmap((void *)descriptor->line_count, sizeof(int));

    close(descriptor->file_fd);
    free(descriptor->child_pids);
    free(descriptor);
}

