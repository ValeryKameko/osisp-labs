#ifndef SLAVE_PROCESS_H
#define SLAVE_PROCESS_H

#include <semaphore.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct {
    int file_fd;
    size_t lines_left;
    size_t lines_batch;
    sem_t *file_lock;
    sem_t *initialized_lock;
    sem_t *events;
    volatile int *line_count;
    volatile bool started;
    pid_t pid;
} slave_process_t, *pslave_process_t;

pslave_process_t slave_process_create(
    char *file_path,
    size_t lines_count,
    size_t lines_batch,
    sem_t *file_lock,
    sem_t *initialized_lock,
    sem_t *events,
    volatile int *line_count

);
void slave_process_start(pslave_process_t descriptor);
void slave_process_destroy(pslave_process_t descriptor);

#endif // SLAVE_PROCESS_H