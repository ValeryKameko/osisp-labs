#ifndef MASTER_PROCESS_H
#define MASTER_PROCESS_H

#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>

typedef struct {
    pid_t group_id;
    int file_fd;
    sem_t * file_lock;
    sem_t * child_initialized_lock;
    sem_t * events;
    volatile int * line_count;
    size_t child_count;
    pid_t * child_pids;
    volatile int processes_finished;
} master_process_t, *pmaster_process_t;

pmaster_process_t master_process_create(size_t child_count);
void master_process_start(pmaster_process_t descriptor);
void master_process_destroy(pmaster_process_t descriptor);

#endif // MASTER_PROCESS_H