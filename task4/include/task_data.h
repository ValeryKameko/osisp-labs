#ifndef TASK_H
#define TASK_H

typedef struct {
    char *from_path;
    char *to_path;
} task_data_t, *ptask_data_t;

ptask_data_t task_data_create(
    const char *from_path, 
    const char *to_path);
void task_data_destroy(ptask_data_t *task);

#endif // TASK_H