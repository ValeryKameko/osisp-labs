#ifndef WORKER_H
#define WORKER_H

#include <pthread.h>
#include <semaphore.h>

struct tag_worker_description;
typedef struct tag_worker_description 
    worker_description_t, 
    *pworker_description_t;

struct tag_worker_pool_description;
typedef struct tag_worker_pool_description 
    worker_pool_description_t, 
    *pworker_pool_description_t;

typedef void (*ptask_func_t)(pworker_description_t worker);
typedef struct tag_worker_description {
    int id;
    pthread_t thread_id;
    void *task_data;
    ptask_func_t task;
    pworker_pool_description_t worker_pool;
    sem_t ready_lock;
} worker_description_t, *pworker_description_t;

typedef void (*ptask_func_t)(pworker_description_t worker);

void worker_initialize(
    pworker_description_t worker, 
    pworker_pool_description_t worker_pool, 
    ptask_func_t task);
void worker_start(pworker_description_t worker);
void worker_notify_newtask(pworker_description_t worker);
void worker_notify_kill(pworker_description_t worker);
void worker_kill_and_join(pworker_description_t worker);
void worker_join(pworker_description_t worker);
void worker_kill_and_waitjoin(pworker_description_t worker);
int  worker_waitjoin(pworker_description_t worker, const struct timespec *wait_time);
void worker_finalize(pworker_description_t worker);

#endif // WORKER_H