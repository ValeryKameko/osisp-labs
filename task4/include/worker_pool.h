#ifndef WORKER_POOL_H
#define WORKER_POOL_H

#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>
#include "worker.h"
#include "task_data.h"

typedef struct tag_worker_pool_description {
    size_t worker_count;
    pworker_description_t workers;

    sem_t free_workers_queue_lock;
    sem_t free_workers_count_lock;
    size_t free_worker_count;
    pworker_description_t *free_workers; 
} worker_pool_description_t, *pworker_pool_description_t;

pworker_pool_description_t worker_pool_create(
    size_t worker_count, 
    ptask_func_t task);
pworker_pool_description_t worker_pool_process_task(
    pworker_pool_description_t worker_pool, 
    void *task_data);
pworker_description_t worker_pool_get_free_worker(
    pworker_pool_description_t worker_pool);
void worker_pool_add_free_worker(
    pworker_pool_description_t worker_pool, 
    pworker_description_t worker);
void worker_pool_destroy(
    pworker_pool_description_t *worker_pool);

#endif // WORKER_POOL_H
