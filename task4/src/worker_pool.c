#define _GNU_SOURCE
#include "worker_pool.h"
#include <stdlib.h>
#include <stdbool.h>
#include <error.h>
#include <errno.h>
#include <signal.h>

pworker_pool_description_t worker_pool_create(
    size_t worker_count, 
    ptask_func_t task)
{
    pworker_pool_description_t pool = (pworker_pool_description_t)malloc(sizeof(worker_pool_description_t));
    pool->worker_count = worker_count;
    pool->workers = (pworker_description_t)calloc(worker_count, sizeof(worker_description_t));
    for (size_t i = 0; i < pool->worker_count; i++)
        worker_initialize(&pool->workers[i], pool, task);

    pool->free_worker_count = 0;
    pool->free_workers = (pworker_description_t *)calloc(worker_count, sizeof(pworker_description_t));
    sem_init(&pool->free_workers_queue_lock, false, 1);
    sem_init(&pool->free_workers_count_lock, false, 0);

    for (size_t i = 0; i < pool->worker_count; i++) {
        pool->workers[i].id = i;
        worker_start(&pool->workers[i]);
    }
    return pool;
}

pworker_pool_description_t worker_pool_process_task(
    pworker_pool_description_t worker_pool, 
    void *task_data)
{
    pworker_description_t worker = worker_pool_get_free_worker(worker_pool);
    worker->task_data = task_data;
    worker_notify_newtask(worker);
}

pworker_description_t worker_pool_get_free_worker(
    pworker_pool_description_t worker_pool)
{
    sem_wait(&worker_pool->free_workers_count_lock);

    worker_pool->free_worker_count--;
    pworker_description_t worker = worker_pool->free_workers[worker_pool->free_worker_count];

    sem_post(&worker_pool->free_workers_queue_lock);
    return worker;
}

void worker_pool_add_free_worker(
    pworker_pool_description_t worker_pool, 
    pworker_description_t worker)
{
    sem_wait(&worker_pool->free_workers_queue_lock);

    worker_pool->free_workers[worker_pool->free_worker_count] = worker;
    worker_pool->free_worker_count++;

    sem_post(&worker_pool->free_workers_queue_lock);
    sem_post(&worker_pool->free_workers_count_lock);
}

void worker_pool_destroy(
    pworker_pool_description_t *pworker_pool)
{
    pworker_pool_description_t worker_pool = *pworker_pool;
    for (size_t i = 0; i < worker_pool->worker_count; i++)
        worker_kill_and_join(&worker_pool->workers[i]);

    sem_destroy(&worker_pool->free_workers_queue_lock);
    sem_destroy(&worker_pool->free_workers_count_lock);

    free(worker_pool->free_workers);
    for (size_t i = 0; i < worker_pool->worker_count; i++)
        worker_finalize(&worker_pool->workers[i]);
    free(worker_pool->workers);
    free(worker_pool);
    *pworker_pool = NULL;
}