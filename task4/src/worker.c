#define _GNU_SOURCE
#include "worker.h"
#include "worker_pool.h"
#include <memory.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <stdio.h>

static void *worker_func(void *data)
{
    pworker_description_t worker = data;
    int result;

    sigset_t sigset_wait;
    result = sigemptyset(&sigset_wait);
    if (result)
        error(1, errno, "sigset_wait empty error %d", result);

    result = sigaddset(&sigset_wait, SIGTERM);
    if (result)
        error(1, errno, "sigset_wait add signal error %d", result);

    result = sigaddset(&sigset_wait, SIGUSR1);
    if (result)
        error(1, errno, "sigset_wait add signal error %d", result);

    int sig;
    sigset_t sigset_old, sigset_block;

    result = sigemptyset(&sigset_block);
    if (result)
        error(1, errno, "sigset_block empty error %d", result);

    result = sigaddset(&sigset_block, SIGUSR1);
    if (result)
        error(1, errno, "sigset_block add signal error");
        
    result = sigaddset(&sigset_block, SIGTERM);
    if (result)
        error(1, errno, "sigset_block add signal error");

    result = pthread_sigmask(SIG_BLOCK, &sigset_block, &sigset_old);    
    if (result) 
        error(1, errno, "sigset_block set signal mask error");

    sem_post(&worker->ready_lock);
    while (true) {
        worker_pool_add_free_worker(worker->worker_pool, worker);
        result = sigwait(&sigset_wait, &sig);
        if (result)
            error(1, errno, "signal wait error");
        if (sig == SIGTERM)
            return NULL;

        worker->task(worker);
    }
    return NULL;
}

void worker_initialize(
    pworker_description_t worker, 
    pworker_pool_description_t worker_pool, 
    ptask_func_t task)
{
    worker->thread_id = pthread_self();
    worker->worker_pool = worker_pool;
    worker->task_data = NULL;
    worker->task = task;
    sem_init(&worker->ready_lock, 0, 0);
}

void worker_start(pworker_description_t worker)
{
    int result;
    pthread_attr_t *thread_attr = calloc(1, sizeof(pthread_attr_t));

    result = pthread_attr_init(thread_attr);
    if (result)
        error(1, 0, "pthread attributes initialization error %d", result);

    result = pthread_create(
        &worker->thread_id, 
        thread_attr, 
        &worker_func, 
        worker);
    if (result)
        error(1, errno, "pthread creation error");

    result = pthread_attr_destroy(thread_attr);
    if (result)
        error(1, errno, "pthread attributes destruction error");
    free(thread_attr);
}

void worker_notify_newtask(pworker_description_t worker)
{
    int result = pthread_kill(worker->thread_id, SIGUSR1);
    if (result)
        error(1, 0, "notify new task error %d", result);
}

void worker_notify_kill(pworker_description_t worker)
{
    int result;
    result = pthread_kill(worker->thread_id, SIGTERM);
    if (result)
        error(1, 0, "notify kill error %d", result);
}

static void timespec_normalize(struct timespec *ts)
{
    const long NSEC_IN_SEC = 1000000000;

    if (ts->tv_nsec > NSEC_IN_SEC) {
        ts->tv_sec += ts->tv_nsec / NSEC_IN_SEC;
        ts->tv_nsec %= NSEC_IN_SEC;
    } else if (ts->tv_nsec < 0) {
        int delta_sec = (1 + NSEC_IN_SEC - ts->tv_nsec) / NSEC_IN_SEC;
        ts->tv_sec -= delta_sec;
        ts->tv_nsec += delta_sec * NSEC_IN_SEC;
    }
}

void worker_kill_and_waitjoin(pworker_description_t worker)
{
    const int TRIES_COUNT = 2;
    struct timespec wait_time = {
        .tv_sec = 2,
        .tv_nsec = 0,
    };
    int tries = TRIES_COUNT;
    sem_wait(&worker->ready_lock);
    worker_notify_kill(worker);
    while (tries > 0) {
        int result = worker_waitjoin(worker, &wait_time);
        if (result == EBUSY) {
            while (nanosleep(&wait_time, &wait_time) && errno == EINTR);
            continue;
        }
        if (result == ETIMEDOUT) {
            pthread_cancel(worker->thread_id);
            continue;
        }
        tries--;
    }
}

int worker_waitjoin(pworker_description_t worker, const struct timespec *wait_time)
{
    struct timespec ts;
    
    clock_gettime(CLOCK_REALTIME, &ts);

    ts.tv_sec += wait_time->tv_sec;
    ts.tv_nsec += wait_time->tv_nsec;

    timespec_normalize(&ts);

    int result;
    result = pthread_timedjoin_np(worker->thread_id, NULL, &ts);
    return result;
}

void worker_kill_and_join(pworker_description_t worker)
{
    sem_wait(&worker->ready_lock);
    worker_notify_kill(worker);
    worker_join(worker);
}

void worker_join(pworker_description_t worker)
{
    pthread_join(worker->thread_id, NULL);
}

void worker_finalize(pworker_description_t worker)
{
    sem_destroy(&worker->ready_lock);
}

