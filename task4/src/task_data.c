#include "task_data.h"
#include <string.h>
#include <malloc.h>
#include <memory.h>

ptask_data_t task_data_create(
    const char *from_path, 
    const char *to_path)
{
    ptask_data_t task_data = (ptask_data_t)malloc(sizeof(task_data_t));
    task_data->from_path = strdup(from_path);
    task_data->to_path = strdup(to_path);
    return task_data;
}

void task_data_destroy(ptask_data_t *task)
{
    free((*task)->from_path);
    free((*task)->to_path);
    free(*task);
    *task = NULL;
}