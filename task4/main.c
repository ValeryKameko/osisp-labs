#define _GNU_SOURCE

#include <ftw.h>
#include <pthread.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <error.h>
#include <errno.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>
#include "worker_pool.h"
#include "task_data.h"

const int MAX_MOPENFD = 10;

void worker_task(pworker_description_t worker) {
    int result;
    ptask_data_t task_data = (ptask_data_t)worker->task_data;
    
    int from_fd = open(task_data->from_path, O_RDONLY | O_EXCL);
    if (from_fd == -1) {
        error(
            0, 
            errno, 
            "error opening file \"%s\", from #%d", 
            task_data->from_path, 
            worker->id);
        task_data_destroy((ptask_data_t *)&worker->task_data);
        return;
    }

    struct stat from_file_stat;

    result = fstat(from_fd, &from_file_stat);
    if (from_fd == -1) {
        error(
            0, 
            errno, 
            "error fstat file \"%s\", from #%d", 
            task_data->from_path,
            worker->id);
        task_data_destroy((ptask_data_t *)&worker->task_data);
        close(from_fd);
        return;
    }
    int to_fd = open(
        task_data->to_path,
        O_WRONLY | O_EXCL | O_CREAT,
        from_file_stat.st_mode);
    if (to_fd == -1 && errno == EEXIST)
        return;
    if (to_fd == -1) {
        error(
            0, 
            errno, 
            "error creating file \"%s\", from #%d", 
            task_data->from_path, 
            worker->id);
        task_data_destroy((ptask_data_t *)&worker->task_data);
        close(from_fd);
        return;
    }

    const size_t BUFFER_SIZE = 1 << 12;
    unsigned char *buffer = (unsigned char *)malloc(BUFFER_SIZE);
    size_t copied_byte_count = 0;

    while (true) {
        ssize_t copy_size = read(from_fd, buffer, BUFFER_SIZE);
        if (copy_size == -1) {
            error(
                0, 
                errno, 
                "error read from file \"%s\", from #%d", 
                task_data->from_path,
                worker->id);
            task_data_destroy((ptask_data_t *)&worker->task_data);
            free(buffer);
            close(to_fd);
            close(from_fd);
            return;
        }
        if (copy_size == 0)
            break;
        ssize_t copied_size = write(to_fd, buffer, copy_size);
        if (copied_size == -1) {
            error(
                0, 
                errno, 
                "error write to file \"%s\", from #%d", 
                task_data->to_path,
                worker->id);
            task_data_destroy((ptask_data_t *)&worker->task_data);
            free(buffer);
            close(to_fd);
            close(from_fd);
            return;
        }
        copied_byte_count += copy_size;
    }
    free(buffer);
    close(to_fd);
    close(from_fd);
    printf(
        "worker #%d[thread #%ld] copied %10ld bytes from \"%s\" to \"%s\"\n", 
        worker->id,
        worker->thread_id,
        copied_byte_count,
        task_data->from_path,
        task_data->to_path);
    task_data_destroy((ptask_data_t *)&worker->task_data);
}

pworker_pool_description_t worker_pool;
char *from_dir;
char *to_dir;

char *get_relative_suffix(const char *path, const char *prefix)
{
    const char *it_path = path;
    const char *it_prefix = prefix;
    while (*it_path && *it_prefix && *it_prefix == *it_path) {
        it_path++;
        it_prefix++;
    }
    if (*it_prefix)
        return NULL;
    return strdup(it_path);
}

char *concat(const char *str1, const char *str2)
{
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    char *buffer = (char *)malloc(len1 + len2 + 1);
    memcpy(buffer, str1, len1);
    memcpy(buffer + len1, str2, len2);
    buffer[len1 + len2] = '\0';
    return buffer;
}

int traverse_directory_handler(
    const char *fpath, 
    const struct stat *sb,
    int typeflag)
{
    int result;
    ptask_data_t task_data;
    char *relative_suffix = get_relative_suffix(fpath, from_dir);
    char *to_file_path = concat(to_dir, relative_suffix);

    switch (typeflag)
    {
        case FTW_F:
            task_data = task_data_create(fpath, to_file_path);
            worker_pool_process_task(worker_pool, task_data);
            break;

        case FTW_D:
            fflush(stdout);
            result = mkdir(to_file_path, sb->st_mode);
            if (result == -1 && errno != EEXIST)
                error(0, errno, "create directory \"%s\" error\n", to_file_path);
            break;

        case FTW_DNR:
            printf("cannot read dir \"%s\"\n", fpath);
            break;

        default:
            break;
    }
    free(relative_suffix);
    free(to_file_path);
    return 0;
}

int main(int argc, char **argv)
{
    if (argc != 3) 
        error(1, 0, "Exactly 2 arguments required");

    int thread_number;
    printf("Enter number of threads: ");
    scanf("%d", &thread_number);

    struct stat dir1_info, dir2_info;
    if (lstat(argv[1], &dir1_info))
        error(1, errno, "error with \"%s\"", argv[1]);
    if (!S_ISDIR(dir1_info.st_mode))
        error(1, 0, "\"%s\" is not dir", argv[1]);
    int result = lstat(argv[2], &dir2_info);
    if (result == -1 && mkdir(argv[2], 0755) == -1)
        error(1, errno, "error with \"%s\"", argv[2]);
    result = lstat(argv[2], &dir2_info);
    if (!S_ISDIR(dir2_info.st_mode))
        error(1, 0, "\"%s\" is not dir", argv[2]);

    from_dir = canonicalize_file_name(argv[1]);
    to_dir = canonicalize_file_name(argv[2]);
    worker_pool = worker_pool_create(thread_number, worker_task);

    ftw(
        from_dir, 
        traverse_directory_handler,
        MAX_MOPENFD);

    worker_pool_destroy(&worker_pool);
    free(from_dir);
    free(to_dir);
    return 0;
}