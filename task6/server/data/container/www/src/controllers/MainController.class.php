<?php

namespace Controller;

use Database\UniversityJournalDatabase;
use Model\StudentModel;
use Model\SubjectModel;
use Model\LessonModel;
use Model\MarkModel;
use View\MainView;

class MainController {
    private $database;
    private $studentModel;
    private $lessonModel;
    private $markModel;

    public function __construct() {
        $this->database = UniversityJournalDatabase::getDatabase();
        $this->studentModel = new StudentModel($this->database->getPDO());
        $this->lessonModel = new LessonModel($this->database->getPDO());
        $this->markModel = new MarkModel($this->database->getPDO());
        $this->subjectModel = new SubjectModel($this->database->getPDO());
    }

    public function handle($options) {
        $students = array_map(function ($entry) {
            return [
                'id' => $entry['id'],
                'name' => $entry['name'],
            ];
        }, $this->studentModel->getAllStudents());

        $lessons = array_map(function ($entry) {
            return [
                'id' => $entry['id'],
                'date' => $entry['date'],
                'subject_name' => $entry['subject_name'],
            ];
        }, $this->lessonModel->getAllLessons());

        $subjects = array_map(function ($entry) {
            return [
                'id' => $entry['id'],
                'name' => $entry['name'],
            ];
        }, $this->subjectModel->getAllSubjects());

        $initial_table = $this->generateInitialTable($subjects, $lessons, $students);
        $processed_table = $this->generateProcessedTable($subjects, $lessons, $students);

        $view = new MainView();
        $view->render([
            'result' => $options['result'] ?? NULL,
            'error' => $options['error'] ?? NULL,
            'lessons' => $lessons,
            'students' => $students,
            'subjects' => $subjects,
            'initial_table' => $initial_table,
            'processed_table' => $processed_table
        ]);
    }

    private function minifyName($name) {
        $words = preg_split('/\s+/', $name, -1, PREG_SPLIT_NO_EMPTY);
        $letters = array_map(function ($word){ return substr($word, 0, 1); }, $words);
        $abbrevation = implode('', $letters);
        return $abbrevation;
    }

    private function generateProcessedTable($subjects, $lessons, $students) {
        $processed_table = [
            'subjects' => [],
            'data' => []
        ];

        $subjectID_to_mapID = [];
        $mapID = 0;
        foreach ($subjects as $subject) {
            $subjectID_to_mapID[$subject['id']] = $mapID;
            $mapID++;
        }

        $subjectMinifier = function($subject) {
            return [
                'name' => $this->minifyName($subject['name']),
                'id' => $subject['id']
            ];
        };

        $processed_table['subjects'] = array_map($subjectMinifier, $subjects);
        foreach ($students as $student) {
            $subjectsAverages = $this->studentModel->getSubjectsAverageMarks($student['id']);
            $minimum_mark = $this->studentModel->getMinimumMark($student['id']);
            $minimum_subjects = $this->studentModel->getMinimumMarkSubjects($student['id']);

            $maximum_mark = $this->studentModel->getMaximumMark($student['id']);
            $maximum_subjects = $this->studentModel->getMaximumMarkSubjects($student['id']);
            $entry = [
                'student' => $student,
                'average_marks' => [],
                'minimum_mark' => ($minimum_mark ? $minimum_mark['min_value'] : ''),
                'minimum_subjects' => array_map($subjectMinifier, $minimum_subjects),
                'maximum_mark' => ($maximum_mark ? $maximum_mark['max_value'] : ''),
                'maximum_subjects' => array_map($subjectMinifier, $maximum_subjects),
            ];

            foreach ($subjects as $subject) {
                $entry['average_marks'][] = NULL;
            }

            foreach ($subjectsAverages as $subjectAverage) {
                $entry['average_marks'][$subjectID_to_mapID[$subjectAverage['id']]] = [
                    'average_value' => number_format($subjectAverage['average_value'], 2)
                ];
            }
            $processed_table['data'][] = $entry;
        }

        return $processed_table;
    }

    private function generateInitialTable($subjects, $lessons, $students) {
        $initial_table = [
            'top_headers' => [],
            'headers' => [],
            'marks' => []
        ];

        $lessonID_to_mapID = [];

        $mapID = 0;

        foreach ($lessons as $lesson) {
            if ((($initial_table['top_headers'][count($initial_table['top_headers']) - 1] ?? NULL)['date'] ?? NULL) !== $lesson['date']) {
                $initial_table['top_headers'][] = [
                    'date' => $lesson['date'],
                    'count' => 1
                ];
            } else {
                $initial_table['top_headers'][count($initial_table['top_headers']) - 1]['count']++;
            }
            $lessonID_to_mapID[$lesson['id']] = $mapID;
            $mapID++;
            $initial_table['headers'][] = [
                'name' => $this->minifyName($lesson['subject_name'])
            ];
        }

        foreach ($students as $student) {
            $entry = [
                'student' => $student,
                'marks' => []
            ];

            $marks = $this->studentModel->getAllStudentMarks($student['id']);
            foreach ($lessons as $lesson) {
                $entry['marks'][] = [
                    'lesson_id' => $lesson['id'],
                    'subject_name' => $lesson['subject_name'],
                    'value' => ''
                ];
            }

            foreach ($marks as $mark) {
                $entry['marks'][$lessonID_to_mapID[$mark['lesson_id']]]['value'] = strval($mark['value']);
            }

            $initial_table['marks'][] = $entry;
        }
        return $initial_table;
    }
}