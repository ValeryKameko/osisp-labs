<?php

namespace Controller;

use Database\UniversityJournalDatabase;
use Model\MarkModel;
use Model\StudentModel;
use Model\LessonModel;
use Model\SubjectModel;
use View\SetMarkPageView;

class SetMarkPageController extends Controller {
    private $database;
    private $studentModel;
    private $lessonModel;
    private $subjectModel;
    private $markModel;

    public function __construct() {
        $this->database = UniversityJournalDatabase::getDatabase();
        $this->studentModel = new StudentModel($this->database->getPDO());
        $this->subjectModel = new SubjectModel($this->database->getPDO());
        $this->lessonModel = new LessonModel($this->database->getPDO());
        $this->markModel = new MarkModel($this->database->getPDO());
    }

    public function handle($options) {
        $students = array_map(function ($entry) {
            return [
                'id' => $entry['id'],
                'name' => $entry['name'],
            ];
        }, $this->studentModel->getAllStudents());

        $lessons = array_map(function ($entry) {
            return [
                'id' => $entry['id'],
                'date' => $entry['date'],
                'subject_name' => $entry['subject_name'],
            ];
        }, $this->lessonModel->getAllLessons());

        $subjects = array_map(function ($entry) {
            return [
                'id' => $entry['id'],
                'name' => $entry['name'],
            ];
        }, $this->subjectModel->getAllSubjects());

        $lesson_dates = [];
        foreach ($lessons as $lesson) {
            $lesson_dates[$lesson['date']] = $lesson_dates[$lesson['date']] ?? [];
            $lesson_dates[$lesson['date']]['date'] = $lesson['date'];
            $lesson_dates[$lesson['date']]['lessons'] = 
                $lesson_dates[$lesson['date']]['lessons'] ?? [];
            $lesson_dates[$lesson['date']]['lessons'][] = $lesson;
        }

        $dates = [];
        foreach ($lesson_dates as $date => $lesson_date) {
            $dates[] = $date;
        }

        $view = new SetMarkPageView();
        $view->render([
            'result' => $options['result'] ?? NULL,
            'error' => $options['error'] ?? NULL,
            'lessons' => $lessons,
            'lesson_dates' => $lesson_dates,
            'students' => $students,
            'dates' => $dates
        ]);
    }
}