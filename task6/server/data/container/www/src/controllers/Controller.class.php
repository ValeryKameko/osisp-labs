<?php

namespace Controller;

use TemplateEngine\Environment;
use TemplateEngine\FilesystemTemplateLoader;

abstract class Controller {
    private static $env;

    public static function redirect($location) {
        header("Location: $location");
        exit(1);
    }
}