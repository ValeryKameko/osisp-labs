<?php

namespace Controller;

use Database\UniversityJournalDatabase;
use Model\MarkModel;
use Model\StudentModel;
use Model\LessonModel;
use Model\SubjectModel;

class StudentController extends Controller {
    private $database;
    private $studentModel;
    private $lessonModel;
    private $markModel;

    public function __construct() {
        $this->database = UniversityJournalDatabase::getDatabase();
        $this->studentModel = new StudentModel($this->database->getPDO());
        $this->lessonModel = new LessonModel($this->database->getPDO());
        $this->markModel = new MarkModel($this->database->getPDO());
    }

    public function setMark($studentId, $lessonId, $mark) {
        $model = new MarkModel($this->database->getPDO(), $lessonId, $studentId);
        $model->setMark($mark);
    }

    public function clearMark($studentId, $lessonId) {
        $model - new MarkModel($this->database->getPDO(), $lessonId, $studentId);
        $model->clearMark();
    }

    public function handleAddStudent($options) {
        $studentName = $options['student_name'] ?? NULL;
        echo "Hello?";
        if (is_null($studentName))
            Controller::redirect('/add_student_page?result=error&error=' . urlencode("Not correct query"));

        echo "Hello?";
        var_dump($this->studentModel->addStudent($studentName));

        Controller::redirect('/add_student_page?result=ok');
    }
}