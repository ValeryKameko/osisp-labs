<?php

namespace View;

class SetMarkPageView extends View {

    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('set_mark_page.tmpl');
        $template->render($parameters);
    }
}
