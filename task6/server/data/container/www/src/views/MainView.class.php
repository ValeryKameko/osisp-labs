<?php

namespace View;

class MainView extends View {

    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('main.tmpl');
        $template->render($parameters);
    }
}
