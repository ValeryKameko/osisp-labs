<?php

namespace View;

use TemplateEngine\Environment;
use TemplateEngine\FilesystemTemplateLoader;

abstract class View {
    protected $templateEngineEnvironment;

    public function __construct() {
        $this->templateEngineEnvironment = new Environment(
            new FilesystemTemplateLoader(dirname(dirname(__DIR__)) . '/template'));
    }

    public abstract function render($parameters);
}