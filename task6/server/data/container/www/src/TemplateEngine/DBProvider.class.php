<?php

namespace TemplateEngine;

interface DBProvider
{
    function get($variable); 
}