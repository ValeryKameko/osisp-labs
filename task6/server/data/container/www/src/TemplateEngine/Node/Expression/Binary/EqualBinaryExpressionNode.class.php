<?php

namespace TemplateEngine\Node\Expression\Binary;

use TemplateEngine\Compiler;

class EqualBinaryExpressionNode extends AbstractBinaryExpressionNode
{
    public function compileOperator(Compiler $compiler)
    {
        $compiler->write('==');
    }
}