<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;
use TemplateEngine\Token;
use TemplateEngine\Node\IfNode;

class IfBlockNodeParser extends AbstractNodeParser
{
    public function __construct()
    {

    }

    public function subparse(Parser $parser)
    {
        $ifBlocks = [];

        $line = $parser->getLine();
        $parser->expect(Token::NAME_TYPE, 'if');
        $ifExpression = $parser->parseExpression();
        $parser->expect(Token::BLOCK_END_TYPE);

        $ifBody = $parser->subparse(false);

        array_push($ifBlocks, [
            'expression' => $ifExpression,
            'body' => $ifBody 
        ]);

        while ($parser->test(Token::NAME_TYPE, 'elseif')) {
            $parser->expect(Token::NAME_TYPE, 'elseif');
            $ifExpression = $parser->parseExpression();
            $parser->expect(Token::BLOCK_END_TYPE);

            $ifBody = $parser->subparse(false);

            array_push($ifBlocks, [
                'expression' => $ifExpression,
                'body' => $ifBody
            ]);
        }
 
        if ($parser->test(Token::NAME_TYPE, 'else')) {
            $parser->expect(Token::NAME_TYPE, 'else');
            $parser->expect(Token::BLOCK_END_TYPE);
            $elseBlock = $parser->subparse(false);
        } else {
            $elseBlock = NULL;
        }
        $parser->expect(Token::NAME_TYPE, 'endif');
        
        return new IfNode($ifBlocks, $line, $elseBlock);
    }
}