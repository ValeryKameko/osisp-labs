<?php

namespace TemplateEngine;

use TemplateEngine\Node\Expression\ConstantExpressionNode;
use TemplateEngine\Node\Expression\NameExpressionNode;
use TemplateEngine\Node\Expression\DictionaryAccessBinaryExpressionNode;
use TemplateEngine\Error\SyntaxError;
use TemplateEngine\Node\Expression\FunctionCallExpressionNode;
use TemplateEngine\Node\Expression\ArrayExpressionNode;


class ExpressionParser
{
    public const ASSOCITIVITY_LEFT = 0;
    public const ASSOCITIVITY_RIGHT = 1;

    private $unaryOperators;
    private $binaryOperators;
    private $parser;

    public function __construct(Environment $env, Parser $parser)
    {
        $this->parser = $parser;
        $this->unaryOperators = $env->getOption('unary_operators');
        $this->binaryOperators = $env->getOption('binary_operators');
    }

    public function parse($precedence = 0)
    {
        $node = $this->processPrimary($precedence);
        while ($this->isBinaryOperator($this->parser->currentToken())) {
            $operator = $this->binaryOperators[$this->parser->currentToken()->getValue()];
            if ($operator['precedence'] < $precedence)
                break;
            $this->parser->next();
            if ($operator['associativity'] === self::ASSOCITIVITY_LEFT)
                $subExpressionNode = $this->parse($operator['precedence'] + 1);
            else
                $subExpressionNode = $this->parse($operator['precedence']);
            
            $operatorNodeClass = $operator['class'];
            $node = new $operatorNodeClass($node, $subExpressionNode, $this->parser->getLine());
        }

        return $node;
    }

    public function processPrimary($precedence = 0)
    {
        $token = $this->parser->currentToken();
        $node = NULL;
        if ($this->isPrimaryExpression($token))
            $node = $this->processPrimaryExpression($precedence);
        elseif ($this->parser->test(Token::PUNCTUATION_TYPE, '(')) {
            $this->parser->next();
            $node = $this->parse();
            $this->parser->expect(Token::PUNCTUATION_TYPE, ')');
        } elseif ($this->parser->test(Token::PUNCTUATION_TYPE, '[')) {
            $elements = [];
            do {
                $this->parser->next();
                $element = $this->parse();
                array_push($elements, $element);
            } while ($this->parser->test(Token::PUNCTUATION_TYPE, ','));
            $this->parser->expect(Token::PUNCTUATION_TYPE, ']');

            $node = new ArrayExpressionNode($elements, $this->parser->getLine());
        }
        return $this->processPostfixExpression($node);
    }

    public function processPrimaryExpression($precedence = 0)
    {
        $token = $this->parser->currentToken();
        $tokenType = $token->getType();
        switch ($tokenType) {
            case Token::STRING_TYPE:
            case Token::NUMBER_TYPE:
                $node = new ConstantExpressionNode($token->getValue(), $token->getLine());
                break;
            case Token::NAME_TYPE:
                if (\strtolower($token->getValue()) === 'true')
                    $node = new ConstantExpressionNode(true, $token->getLine());
                elseif (\strtolower($token->getValue()) === 'false')
                    $node = new ConstantExpressionNode(false, $token->getLine());
                elseif (\strtolower($token->getValue()) === 'null')
                    $node = new ConstantExpressionNode(null, $token->getLine());
                else
                    $node = new NameExpressionNode($token->getValue(), $token->getLine());
                break;
        }
        $this->parser->next();
        return $node;
    }

    public function processPostfixExpression($node)
    {
        $token = $this->parser->currentToken();
        while (true) {

            if ($this->parser->test(Token::PUNCTUATION_TYPE, '[')) {
                $this->parser->next();
                $keyExpression = $this->parse();
                $this->parser->expect(Token::PUNCTUATION_TYPE, ']');
                $node = new DictionaryAccessBinaryExpressionNode($node, $keyExpression, $this->parser->getLine());
            }
            elseif ($this->parser->test(Token::PUNCTUATION_TYPE, '(')) {
                if (!($node instanceof NameExpressionNode))
                    throw new SyntaxError(
                        'Function name must be constant name expression', 
                        $this->parser->getLine(),
                        $this->parser->getTokenStream()->getSource());
    
                $functionName = $node;
                $parameters = [];
                do {
                    $this->parser->next();
                    array_push($parameters, $this->parse());
                } while ($this->parser->test(Token::PUNCTUATION_TYPE, ','));
                $this->parser->expect(Token::PUNCTUATION_TYPE, ')');
                $node = new FunctionCallExpressionNode($functionName, $parameters, $this->parser->getLine());
            }
            else
                break;
        }

        return $node;
    }

    private function isUnaryOperator($token)
    {
        return 
            ($token->test(Token::OPERATOR_TYPE)) &&
            (isset($this->unaryOperators[$token->getValue()]));
    }

    private function isBinaryOperator($token)
    {
        return 
            ($token->test(Token::OPERATOR_TYPE)) &&
            (isset($this->binaryOperators[$token->getValue()]));
    }

    private function isPrimaryExpression($token)
    {
        return $token->test([
            Token::STRING_TYPE,
            Token::NUMBER_TYPE,
            Token::NAME_TYPE
        ]);
    }
}