<?php

namespace Model;

class StudentModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function isExists($studentId) {
        $sql = 'SELECT COUNT(*)
                FROM `student`
                WHERE `student`.`id` = :student_id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':student_id' => $studentId
        ]);
        return $query->fetch(\PDO::FETCH_ASSOC) != 0;
    }

    public function getAllStudents() {
        $sql = 'SELECT `student`.*
                FROM `student`';
        $query = $this->db->prepare($sql);
        $query->execute([]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getSubjectsAverageMarks($studentId) {
        $sql = 'SELECT `subject`.`id`, AVG(`mark`.`value`) AS `average_value`
                FROM `student`
                INNER JOIN `mark` ON `student`.`id` = `mark`.`student_id`
                INNER JOIN `lesson` ON `lesson`.`id` = `mark`.`lesson_id`
                INNER JOIN `subject` ON `subject`.`id` =  `lesson`.`subject_id`
                WHERE
                    `student`.`id` = :student_id
                GROUP BY `subject`.`id`';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':student_id' => $studentId
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function getMinimumMark($studentId) {
        $sql = 'SELECT MIN(`mark`.`value`) AS `min_value`
                FROM `student`
                INNER JOIN `mark` ON `student`.`id` = `mark`.`student_id`
                INNER JOIN `lesson` ON `lesson`.`id` = `mark`.`lesson_id`
                INNER JOIN `subject` ON `subject`.`id` =  `lesson`.`subject_id`
                WHERE
                    `student`.`id` = :student_id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':student_id' => $studentId
        ]);

        return $query->fetch(\PDO::FETCH_ASSOC);    
    }

    public function getMaximumMark($studentId) {
        $sql = 'SELECT MAX(`mark`.`value`) AS `max_value`
                FROM `student`
                INNER JOIN `mark` ON `student`.`id` = `mark`.`student_id`
                INNER JOIN `lesson` ON `lesson`.`id` = `mark`.`lesson_id`
                INNER JOIN `subject` ON `subject`.`id` =  `lesson`.`subject_id`
                WHERE
                    `student`.`id` = :student_id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':student_id' => $studentId
        ]);
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function getMaximumMarkSubjects($studentId) {
        $maxMark = $this->getMaximumMark($studentId);
        if (!$maxMark)
            return [];
        return $this->getMarkSubjects($studentId, $maxMark['max_value']);
    }

    public function getMinimumMarkSubjects($studentId) {
        $minMark = $this->getMinimumMark($studentId);
        if (!$minMark)
            return [];
        return $this->getMarkSubjects($studentId, $minMark['min_value']);
    }

    public function getMarkSubjects($studentId, $mark) {
        $sql = 'SELECT `subject`.`id`, `subject`.`name`
                FROM `student`
                INNER JOIN `mark` ON `student`.`id` = `mark`.`student_id`
                INNER JOIN `lesson` ON `lesson`.`id` = `mark`.`lesson_id`
                INNER JOIN `subject` ON `subject`.`id` =  `lesson`.`subject_id`
                WHERE
                    `student`.`id` = :student_id AND
                    `mark`.`value` = :mark
                GROUP BY `subject`.`id`';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':student_id' => $studentId,
            ':mark' => $mark
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllStudentMarks($studentId) {
        $sql = 'SELECT 
                    `lesson`.`id` AS `lesson_id`, 
                    `subject`.`name` AS `subject_name`, 
                    `subject`.`id` AS `subject_id`,
                    `mark`.`value` AS `value`,
                    `lesson`.`date`
                FROM `lesson`
                INNER JOIN `mark` ON `mark`.`lesson_id` = `lesson`.`id`
                INNER JOIN `subject` ON `subject`.`id` = `lesson`.`subject_id`
                INNER JOIN `student` ON `student`.`id` = `mark`.`student_id`
                WHERE
                    `student`.`id` = :student_id
                ORDER BY 
                    `lesson`.`date` ASC,
                    `lesson`.`id` ASC';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':student_id' => $studentId
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function addStudent($studentName ) {
        $sql = 'INSERT INTO `student` (`name`)
                VALUE (:name)';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':name' => $studentName
        ]);
    }
}