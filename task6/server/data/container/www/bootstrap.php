<?php

include_once __DIR__ . '/src/Autoloader.php';

Autoloader::setBaseDirectory(__DIR__ . '/src');
Autoloader::addPrefix('TemplateEngine', 'TemplateEngine');
Autoloader::addPrefix('Model', 'models');
Autoloader::addPrefix('Controller', 'controllers');
Autoloader::addPrefix('View', 'views');
Autoloader::addPrefix('Database', 'databases');
Autoloader::addPrefix('', '');
Autoloader::register();

$router = new Router();

$router->addRoute('/^\/?$/', function(...$routeParams) {
    $controller = new Controller\MainController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/set_mark_page.*$/', function(...$routeParams) {
    $controller = new Controller\SetMarkPageController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/set_mark.*$/', function(...$routeParams) {
    $controller = new Controller\MarkController();
    $controller->handleSetMark($_POST);
});

$router->addRoute('/^\/clear_mark_page.*$/', function(...$routeParams) {
    $controller = new Controller\ClearMarkPageController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/clear_mark.*$/', function(...$routeParams) {
    $controller = new Controller\MarkController();
    $controller->handleClearMark($_POST);
});

$router->addRoute('/^\/add_student_page.*$/', function(...$routeParams) {
    $controller = new Controller\AddStudentPageController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/add_student.*$/', function(...$routeParams) {
    $controller = new Controller\StudentController();
    $controller->handleAddStudent($_POST);
});


$router->addRoute('/^.*$/', function(...$routeParams) {
});


$router->route();