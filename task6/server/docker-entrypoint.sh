#!/bin/bash

adduser "${USER_NAME}" --disabled-password --gecos "First Last,RoomNumber,WorkPhone,HomePhone"
echo "${USER_NAME}:${USER_PASSWORD}" | chpasswd

cp -R /home/data/ /home/user

mkdir -p /var/run/sshd
mkdir -p "/home/${USER_NAME}/.ssh"

cat "/home/${USER_NAME}/data/keys/id_ed25519_temp.pub" >> "/home/${USER_NAME}/.ssh/authorized_keys"

echo "PubkeyAuthentication yes" >> "/etc/ssh/sshd_config"

chown "${USER_NAME}" -R "/home/${USER_NAME}/.ssh"
chmod 700 "/home/${USER_NAME}/.ssh"
chmod 640 "/home/${USER_NAME}/.ssh/*"

groupadd docker
usermod -aG docker "$USER_NAME"

service docker start

/usr/sbin/sshd -D
