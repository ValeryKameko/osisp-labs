FROM ubuntu

RUN apt-get update && \
    apt-get install -y openssh-server

RUN apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    iptables

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" && \
    apt update && \
    apt-cache policy docker-ce && \
    apt install -y docker-ce

RUN curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose && \
    ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

COPY docker-entrypoint.sh /usr/share/bin/docker-entrypoint.sh
RUN chmod +x /usr/share/bin/docker-entrypoint.sh

COPY sshd_config /etc/ssh/sshd_config

CMD [ "/usr/share/bin/docker-entrypoint.sh" ]
